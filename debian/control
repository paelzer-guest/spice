Source: spice
Section: misc
Priority: optional
Maintainer: Liang Guo <guoliang@debian.org>
Uploaders: Michael Tokarev <mjt@tls.msk.ru>
Build-Depends:
 debhelper (>= 10),
 gstreamer1.0-libav,
 gstreamer1.0-plugins-base,
 gstreamer1.0-plugins-good,
 gstreamer1.0-plugins-ugly,
 gstreamer1.0-tools,
 libcacard-dev,
 libglib2.0-dev (>= 2.28),
 libgstreamer-plugins-base1.0-dev,
 libgstreamer1.0-dev,
 libjpeg-dev,
 liblz4-dev,
 libopus-dev (>= 0.9.14),
 libpixman-1-dev (>= 0.17.7),
 libsasl2-dev,
 libspice-protocol-dev (>= 0.12.13),
 libssl-dev,
 pkg-config,
 python,
 python-pyparsing,
 python-six,
 liborc-0.4-dev,
Standards-Version: 4.1.1
Homepage: http://www.spice-space.org
Vcs-Git: https://anonscm.debian.org/git/collab-maint/spice.git
Vcs-Browser: https://anonscm.debian.org/gitweb/?p=collab-maint/spice.git

Package: libspice-server1
Section: libs
Architecture: alpha amd64 arm64 armel armhf i386 mips64el mipsel ppc64el sh4 x32
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends:
 gstreamer1.0-libav,
 gstreamer1.0-plugins-base,
 gstreamer1.0-plugins-good,
 gstreamer1.0-plugins-ugly,
Breaks:
 libspice-client-glib-2.0-1 (<= 0.12-2),
 libspice-client-gtk-2.0-1 (<= 0.12-2),
 libspice-client-gtk-3.0-1 (<= 0.12-2),
 python-spice-client-gtk (<= 0.12-2),
 spice-client-gtk (<= 0.12-2),
Description: Implements the server side of the SPICE protocol
 The Simple Protocol for Independent Computing Environments (SPICE) is
 a remote display system built for virtual environments which allows
 you to view a computing 'desktop' environment not only on the machine
 where it is running, but from anywhere on the Internet and from a wide
 variety of machine architectures.
 .
 This package contains the run-time libraries for any application that
 wishes to be a SPICE server.

Package: libspice-server-dev
Section: libdevel
Architecture: alpha amd64 arm64 armel armhf i386 mips64el mipsel ppc64el sh4 x32
Depends:
 libspice-protocol-dev (>= 0.12.10~),
 libspice-server1 (= ${binary:Version}),
 ${misc:Depends},
Suggests: pkg-config
Description: Header files and development documentation for spice-server
 The Simple Protocol for Independent Computing Environments (SPICE) is
 a remote display system built for virtual environments which allows
 you to view a computing 'desktop' environment not only on the machine
 where it is running, but from anywhere on the Internet and from a wide
 variety of machine architectures.
 .
 This package contains the header files, static libraries and development
 documentation for spice-server.
